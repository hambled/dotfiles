local defOpts = {silent = true, noremap = true}

-- Fix exit with Ctrl-c.
vim.keymap.set('i', '<C-C>'      , '<Esc>'       , defOpts);

-- Select.
vim.keymap.set('n', '<S-Left>'   , 'vh'          , defOpts);
vim.keymap.set('i', '<S-Left>'   , '<Esc>v'      , defOpts);
vim.keymap.set('v', '<S-Left>'   , 'h'           , defOpts);
vim.keymap.set('n', '<S-Right>'  , 'vl'          , defOpts);
vim.keymap.set('i', '<S-Right>'  , '<Esc>lv'     , defOpts);
vim.keymap.set('v', '<S-Right>'  , 'l'           , defOpts);
vim.keymap.set('n', '<S-Up>'     , 'vk'          , defOpts);
vim.keymap.set('i', '<S-Up>'     , '<Esc>vk'     , defOpts);
vim.keymap.set('v', '<S-Up>'     , 'k'           , defOpts);
vim.keymap.set('n', '<S-Down>'   , 'vj'          , defOpts);
vim.keymap.set('i', '<S-Down>'   , '<Esc>lvj'    , defOpts);
vim.keymap.set('v', '<S-Down>'   , 'j'           , defOpts);
vim.keymap.set('n', '<C-S-Up>'   , 'Vk'          , defOpts);
vim.keymap.set('i', '<C-S-Up>'   , '<Esc>Vk'     , defOpts);
vim.keymap.set('v', '<C-S-Up>'   , 'k'           , defOpts);
vim.keymap.set('n', '<C-S-Down>' , 'Vj'          , defOpts);
vim.keymap.set('i', '<C-S-Down>' , '<Esc>Vj'     , defOpts);
vim.keymap.set('v', '<C-S-Down>' , 'j'           , defOpts);
vim.keymap.set('n', '<C-S-Left>' , 'vb'          , defOpts);
vim.keymap.set('i', '<C-S-Left>' , '<Esc>vb'     , defOpts);
vim.keymap.set('n', '<C-S-Right>', 've'          , defOpts);
vim.keymap.set('i', '<C-S-Right>', '<Esc>lve'    , defOpts);
vim.keymap.set('n', '<A-S-Up>'   , '<C-v>k'      , defOpts);
vim.keymap.set('i', '<A-S-Up>'   , '<Esc>l<C-v>k', defOpts);
vim.keymap.set('v', '<A-S-Up>'   , 'k'           , defOpts);
vim.keymap.set('n', '<A-S-Down>' , '<C-v>j'      , defOpts);
vim.keymap.set('i', '<A-S-Down>' , '<Esc>l<C-v>j', defOpts);
vim.keymap.set('v', '<A-S-Down>' , 'j'           , defOpts);

-- Move line.
vim.keymap.set('n', '<C-A-Up>'  , ':t-1<CR>'       , defOpts);
vim.keymap.set('i', '<C-A-Up>'  , '<Esc>:t-1<CR>gi', defOpts);
vim.keymap.set('v', '<C-A-Up>'  , ":t '<-1<CR>gv"  , defOpts);
vim.keymap.set('n', '<C-A-Down>', ':t.<CR>'        , defOpts);
vim.keymap.set('i', '<C-A-Down>', '<Esc>:t.<CR>gi' , defOpts);
vim.keymap.set('v', '<C-A-Down>', ":t '>.<CR>gv"   , defOpts);

-- Copy line.
vim.keymap.set('n', '<A-Up>'    , ':m-2<CR>=='       , defOpts);
vim.keymap.set('i', '<A-Up>'    , '<Esc>:m-2<CR>==gi', defOpts);
vim.keymap.set('v', '<A-Up>'    , ":m '<-2<CR>gv=gv" , defOpts);
vim.keymap.set('n', '<A-Down>'  , ':m+<CR>=='        , defOpts);
vim.keymap.set('i', '<A-Down>'  , '<Esc>:m+<CR>==gi' , defOpts);
vim.keymap.set('v', '<A-Down>'  , ":m '>+1<CR>gv=gv" , defOpts);

