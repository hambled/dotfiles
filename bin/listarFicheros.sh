#!/bin/bash

LIST=listaFicheros.txt

while [ -f $LIST ]; do
    LIST="tmp$LIST"
done

LONGEST=0
for i in *; do
    if [ ${#i} -gt $LONGEST ]; then
        LONGEST=${#i}
    fi
done

for i in *; do
    perl -E "print ' ' x $(($LONGEST - ${#i}))" >> $LIST
    echo "$i;$i" >> $LIST
done

type subl &>/dev/null && subl $LIST
