#!/bin/bash

LIST=listaFicheros.txt

while [ -f "tmp$LIST" ]; do
    LIST="tmp$LIST"
done

if [ ! -f $LIST ]; then
    exit -1
fi

while read f; do
    if [ "${f/;*}" != "${f/*;}" ]; then
        DST="${f/*;}"
        while [ -f "$DST" ]; do
            DST="_$DST"
        done
        mv "${f/;*}" "$DST"
    fi
done<$LIST

rm $LIST
