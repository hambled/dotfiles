# Introducción
Este *dotfiles* está fuertemente centrado en
[*bash*](https://www.gnu.org/software/bash/ "Sitio oficial de GNU Bash"),
[*fzf*](https://github.com/junegunn/fzf/wiki "Command-line Fuzzy Finder (github)") y
[*tmux*](https://github.com/tmux/tmux/wiki "Terminal Multiplexer").  
Sin estas herramientas, este *dotfiles* no tiene mucho sentido.


# Vídeo
Aquí dejo un vídeo explicativo de este [*dotfiles*](https://gitlab.com/hambled/dotfiles "Repositorio oficial").

[![](http://img.youtube.com/vi/4Wv9-cuhVes/0.jpg)](http://www.youtube.com/watch?v=4Wv9-cuhVes "Youtube McA Dotfiles")

# Mejoras
- [ ] Separar el `.bash_aliases` en pequeñas unidades más manejables.
- [ ] Crear script/menú de instalación/configuración.
- [ ] Mejorar algunas funciones (como la de `ccd()`) para que sea más modular y parametrizable.
- [ ] Extender las funcionalidades del [`ps1_builder`](./.bash_aliases.d/PS1_builder) para permitir construir *prompts* nuevos.
- [ ] Crear menú de *tmux* para cargar las configuraciones en vez de usar *buffers*.
- [ ] Permitir de forma interactiva que alias cargar en un *buffer* de *tmux*.

# Instalación
***ToDo***.

# *Prompt* (`$PS1`)
## Configuración
Hay para elegir 3 versiones de los *prompts* fácilmente intercambiables y configurables.

Una vez cargado el [.bashrc](./.bashrc), solo hay que ejecutar una de las 3 opciones:
```bash
PS1=$PS1_OPT1
PS1=$PS1_OPT2
PS1=$PS1_OPT3
```

Es posible cambiar los colores de estos *promts* redefiniendo las siguientes variables
(usando códigos de color [ansi](https://en.wikipedia.org/wiki/ANSI_escape_code "Wiki de ANSI")):
```bash
PS1_HOST_COLOR=
PS1_DATE_COLOR=
PS1_NOTHIST_COLOR=
PS1_EXECTIME_COLOR=
PS1_UID_COLOR=
PS1_DIRS_COLOR=
PS1_BANNER_COLOR=
PS1_DEFAULT_COLOR=
PS1_AT_COLOR=
PS1_USER_COLOR=
PS1_RETVAL_COLOR=
PS1_WORKDIR_COLOR=
PS1_INPUT_COLOR=   # Experimental.
```

El mejor punto de partida es usando el [`ps1_builder`](./.bash_aliases.d/PS1_builder) (ver más abajo).


# Lanzadores desde *tmux*
***ToDo***.

# Funciones y alias más destacables (`bash`)
## `cd` + `fzf`: `ccd`
![Demo ccd](https://gitlab.com/hambled/dotfiles/-/raw/a2bc5f0579dc45ac8e2b100c4d6403f563a9b23c/media/ccd.gif)

Se trata de una herramienta versátil que se puede usar tanto para saltar rápido entre rutas como para muchas otras
tareas cotidianas.

> **Nota**:  
Está pensado para ser lanzado usando la combinación de teclas `C-a`.

## File + `fzf`: `f-fzf`
Permite seleccionar uno o más fichero o carpetas dentro de la ruta actual.

> **Nota**:  
Pulsa `C-f` para lanzarlo de forma rápida.  

## `bash_jump`
![bash_jump animación](https://gitlab.com/hambled/dotfiles/-/raw/a2bc5f0579dc45ac8e2b100c4d6403f563a9b23c/media/bash_jump.gif)

Permite saltar de forma rápida a cualquier posición dentro de la línea de comandos.

> **Nota**:  
Pulsa `C-g` para lanzarlo de forma rápida.  

## `ps1_builder`
![ps1_builder demo](https://gitlab.com/hambled/dotfiles/-/raw/a5cded429e52664efbaf3a12e8b17a489dc70dfd/media/ps1.gif)

Permite elegir y cambiar los colores de los *prompts*.
> **En construcción:**  
De momento solo se puede cambiar los colores de los `$PS1_OPTn` o seleccionar uno de ellos.  
A futuro es posible que se pueda construir nuevos de manera sencilla.

```bash
# Crear una definición de variables sin aplicar cambios.
ps1_builder

# O bien aplicar los cambios al vuelo:
ps1_builder_

# O bien randomizar de forma semialeatoria el PS1 actual:
ps1_builder__
```
> **Nota**:  
Es importante cargar primero la función [`paleta256`](./.bash_aliases.d/paleta256) para poder usar el `ps1_builder__`.


## Paleta [8-bits de ansi](https://en.wikipedia.org/wiki/ANSI_escape_code#8-bit): `paleta256`
![Demo paleta256](https://gitlab.com/hambled/dotfiles/-/raw/a2bc5f0579dc45ac8e2b100c4d6403f563a9b23c/media/paleta256.gif)

Permite explorar y construir una paleta de colores.  
La herramienta permite customizar la paleta de tal forma que luego se puede proporciona una función (*snippet*) que puede reconstruir
la misma paleta pasándole como parámetro el número de la posición que ocupa dentro de la paleta.

Ejemplo de uso:
```bash
# Modo gráfico:
paleta256

# Paleta rápida:
. <(PNAME=lila paleta256 5 6 100 195 2>/dev/null)
for i in {0..10}; do
    color=$(lila $i)
    echo "${color}El color $i es"$'\e[m: '${color@Q}
done
```

## `history` + `fzf`: `hf`
![Demo hist+fzf](https://gitlab.com/hambled/dotfiles/-/raw/a2bc5f0579dc45ac8e2b100c4d6403f563a9b23c/media/hf.gif)

Permite usar `fzf` con el historial.

> **Nota**:  
Pulsa `C-h` (ó `C-<RETROCESO>`) para lanzarlo de forma rápida.

## *Ultimate Plumber* (bash): `up_`
![Demo up_](https://gitlab.com/hambled/dotfiles/-/raw/a2bc5f0579dc45ac8e2b100c4d6403f563a9b23c/media/up.gif)

> **Nota**:  
Pulsa `C-x` + `C-p` para lanzarlo de forma rápida.  
Reemplazo personal en bash para la app de [Ultimante Plumber](https://github.com/akavel/up "Github del proyecto oficial").

## Uncomment: `uc`
Función que permite descomentar un fichero a la vez que eliminar las líneas vacías.  

> **Importante**:  
Esta función solo descomenta los comentarios que se sitúan al principio de línea.  
No se tienen en cuenta los carácteres invisibles.

Forma de uso:
```bash
# Ejecución básica.
# Descomenta todas las líneas que empiezan por '#'.
uc <fichero>

# Cambiar el patrón del comentario.
# Ej: Descomentar todo lo que empieza por '#' ó ';'
uc <fichero> '#|;'

# Usado como tubería:
<comando> | uc
```

## Print indentation: `pi`
![Demo pi](https://gitlab.com/hambled/dotfiles/-/raw/a2bc5f0579dc45ac8e2b100c4d6403f563a9b23c/media/pi.gif)

Estima dónde podrían estar las indentaciones de una entrada de texto e imprime una línea gris sobre ellas.

## Regex Rang Num: `rrnum`
![Demo pi](https://gitlab.com/hambled/dotfiles/-/raw/a2bc5f0579dc45ac8e2b100c4d6403f563a9b23c/media/rrnum.gif)


Permite obtener la expresión regular entre dos números enteros positivos.

## Regex Rang IP: `rrip`
![Demo pi](https://gitlab.com/hambled/dotfiles/-/raw/a2bc5f0579dc45ac8e2b100c4d6403f563a9b23c/media/rrip.gif)

Permite obtener la expresión regular entre 2 ip.  
> **Nota**:  
Internamente usa `rrnum`.

## `grep-ip`
![Demo pi](https://gitlab.com/hambled/dotfiles/-/raw/a2bc5f0579dc45ac8e2b100c4d6403f563a9b23c/media/grep-ip.gif)

Usando la función de `rrip`, ejecuta un `grep` buscando:

* O bien todas las ips dentro de un rango.
* O todas las ips dentro de una `<ip>`/`<mascara>`.

## Command finder: `command-fzf`
![Demo pi](https://gitlab.com/hambled/dotfiles/-/raw/a2bc5f0579dc45ac8e2b100c4d6403f563a9b23c/media/cmd.gif)

> **Nota**:  
Pulsa `C-e` para lanzarlo de forma rápida.

Herramienta que permite listar todos los comandos, alias, funciones, etc disponibles desde la línea de comandos.  
Además, en la preview, se visualizan alguna información útil sobre el comando actual.

Pulsando `C-v` permite explorar todas las variables definidas en la sesión actual.  
Pulsando `C-h` Se puede explorar las entradas de manual para el comando seleccionado.  
Pulsando `C-e` se puede explorar todas las entradas de `man` disponibles.

## Show remote cert: `show_remote_cert`
![Demo pi](https://gitlab.com/hambled/dotfiles/-/raw/a2bc5f0579dc45ac8e2b100c4d6403f563a9b23c/media/show_rc.gif)

Pequeña utilidad para inspeccionar de forma rápida el certificado público de un sitio remoto.

## `kubectl explain` + `fzf`: `kexplain`
Permite explorar la *API* de *kubernetes* y construir, de forma limitada, un fichero *YAML*.

**ToDo**: Doc.