# enable color support of ls and also add handy aliases
if [[ -x /usr/bin/dircolors ]]; then
    if [[ -r ~/.dircolors ]]; then
        # Load custom colors.
        eval "$(dircolors -b ~/.dircolors)"
    else
        eval "$(dircolors -b)"
    fi

    alias ls='ls --color=auto'
fi

bind '"\C-k":clear-display'

command -v tput &>/dev/null && {
    flash_(){
        local sleep=${1//[^0-9.]/}
        local color=${2:-15}
        tput smcup
        echo -en "\e[H\e[48;5;${color}m\e[2J"
        sleep ${sleep:-0.1}
        tput rmcup
    }
}

alias grep=' grep  --color=auto'
alias rgrep='grep -r'

alias    egrep-grey="GREP_COLOR='1;30' grep -E --color=always"
alias     egrep-red="GREP_COLOR='1;31' grep -E --color=always"
alias   egrep-green="GREP_COLOR='1;32' grep -E --color=always"
alias  egrep-yellow="GREP_COLOR='1;33' grep -E --color=always"
alias    egrep-blue="GREP_COLOR='1;34' grep -E --color=always"
alias egrep-magenta="GREP_COLOR='1;35' grep -E --color=always"
alias    egrep-cyan="GREP_COLOR='1;36' grep -E --color=always"
alias   egrep-white="GREP_COLOR='1;37' grep -E --color=always"

export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

if command -v eza &> /dev/null; then
    alias ll='eza -laghHF --git'
    HISTIGNORE+=':ll -T'
else
    alias ll='ls -lahF'
fi


command -v nvim >/dev/null && : ${EDITOR:=nvim}
command -v vim  >/dev/null && : ${EDITOR:=vim}
command -v vi   >/dev/null && : ${EDITOR:=vi}
export EDITOR

# Prompt a question with default option.
# Eg: questionYN "default it's NO"  n
# Eg: questionYN "default it's YES"
questionYN(){
    local opt=.
    local color=
    local SN SNP
    local timeout=${QUESTION_TIMEOUT//[^0-9]/}

    [[ -t 1 ]] && color=1

    if [[ "${2,,}" == n ]]; then
        SN=n
        SNP='(y/N)'
    else
        SN=y
        SNP='(Y/n)'
    fi
    printf '%s %s: ' "$1" "$SNP"
    while [[ "${opt//[ynYN]/}" ]]; do
        IFS=$'\n' read -srn1 ${timeout:+ -t$timeout} opt
        [[ $? -eq 0 && $opt ]] || opt="$SN"
        case $color${opt,} in
            1y) echo -e "\e[1;32my\e[m";;
            1n) echo -e "\e[1;31mn\e[m";;
           y|n) echo ${opt,}           ;;
        esac
    done
    [[ ${opt,} == y ]]
}

# date_ [<filtro> [<parametro1> ...]]
# v1.0
date_(){
    local zones="$(find /usr/share/zoneinfo | sed 's#/usr/share/zoneinfo/##' | grep -i "${1:-.}" | sort | uniq)"
    local n=1
    local m
    shift

    [ -z "$zones" ] && return 1
    while read i; do
        echo -e "\e[36m[$n]\e[m" $i
        n=$(($n + 1))
    done <<<$zones
    n=$(echo "$zones" | wc -l)
    read -p $'\e[32mHora de:\e[m ' m
    [ -z "$m" ] && return
    [ $m -gt 0 -a $m -le $n ] && {
        local tz=$(echo "$zones" | head -n$m | tail -n1)
        TZ=$tz date $@
    }
}

# Pasar a segundos.
# toSec [{<num>[dDhHmMsS]}...]
# Ej:
#   toSec 1d1h1m1
#   90061
# v1.0
toSec(){
    local s=${1,,}s
    s=${s//d/*24h}
    s=${s//h/*60m}
    s=${s//m/*60s}
    s=${s//s/+}
    echo $((${s}0))
}
export -f toSec

# Countdown.
# Bug: No se visualizan los días cuando se pasa de 24h.
# Dependencia: función "toSec".
# v1.1
countdown(){
    command -v toSec &> /dev/null || return 1
    local end
    local opt
    local i
    end=$(toSec "$1") || return 1
    end=$(($(date +%s) + $end))
    date
    for (( i=$(date +%s); i < $end; i=$(date +%s) )); do
        echo -ne "$(date -u --date @$(($end - $i)) +%T)       \r"
        read -st1 -n1 opt
        [[ $opt =~ [sS] ]] && break
        [[ $opt =~ [pP] ]] && {
            echo -ne "-- PAUSED --\r"
            read -sn1
            end=$(($end + $(date +%s) - $i))
        }
    done
    echo -ne '\007'
    date
}

# Countup.
# Bug: No se visualizan los días cuando se pasa de 24h.
# Dependencia: función "toSec".
# v1.0
countup(){
    local opt
    local start=$(date +%s)
    local pause
    local paused=0
    while :; do
        echo -ne "$(date -u --date @$(($(date +%s) - $start)) +%T)       \r"
        read -st1 -n1 opt
        [[ $opt =~ [qQ] ]] && break
        [[ $opt =~ [pP] ]] && {
            echo -ne "-- PAUSED --\r"
            pause=$(date +%s)
            read -sn1
            pause=$(($(date +%s) - $pause))
            paused=$(($paused + $pause))
            start=$(($start + $pause))
            echo -e "\e[1;32m$(date -u --date @$(($(date +%s) - $start)) +%T)\e[m        "
            echo -e "\e[31m$(date -u --date @$pause +'%T') \e[1;31m$(date -u --date @$paused +'%T')\e[m"
        }
    done
    echo -e '\007'
}

coffee(){
    command -v countdown   &> /dev/null || return 1
    command -v notify-send &> /dev/null || return 2
    [[ $# -lt 1 ]] && {
        >&2 echo Falta el parametro de tiempo.
        return 3
    }

    local segundos="$1"
    shift
    local mensaje="${@:-Coffee time!}"

    echo "Se mostrará el mensaje:"
    echo $'\e[32m'"$mensaje"$'\e[m'
    countdown $segundos && {
        notify-send -i face-monkey -u critical "Coffee say:" "$mensaje"
        pgrep -a mocp &> /dev/null && {
            mocp -P &> /dev/null
        }
    }
}

# Calcular la letra de un DNI o NIE extranjero residente.
dni(){
    local resto
    local letras=(T R W A G M Y F P D X B N J Z S Q V H L C K E)
    local letraIn letraOut
    local dniIn dniOut

    dniIn=${1^^}
    dniIn=${dniIn//[^0-9A-Z]/}

    [[ $dniIn =~ ^[XYZ]?[0-9]+[A-Z]?$ ]] || return 2

    [[ ${#dniIn} -eq 9 ]] && {
        letraIn=${dniIn: -1}
        [[ ${letraIn/[A-Z]/} ]] && return 3
        dniIn=${dniIn:0:-1}
    }

    [[ ${#dniIn} -eq 8 ]] || return 4

    resto=$(tr [XYZ] [012] <<<$dniIn)
    resto=$(("10#$resto" % 23))
    letraOut=${letras[$resto]}

    dniOut=$dniIn$letraOut
    [[ -z $NOCOLOR && -n $letraIn ]] && {
        if [[ $letraIn == $letraOut ]]; then
            dniOut=$'\e[1;32m'$dniOut
        else
            dniOut=$'\e[1;31m'$dniOut
        fi
        dniOut+=$'\e[m'
    }

    echo $dniOut

    [[ -z $letraIn || $letraIn == $letraOut ]]
}
export -f dni

urldecode(){
    : "${*//+/ }"
    echo -e "${_//%/\\x}"
}
export -f urldecode


alias ,='pushd .'
alias ,-='pushd -'
alias .-='cd -'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'
alias .......='cd ../../../../../..'
alias ........='cd ../../../../../../..'
alias .........='cd ../../../../../../../..'
alias ..........='cd ../../../../../../../../..'
alias ...........='cd ../../../../../../../../../..'
alias ............='cd ../../../../../../../../../../..'
alias d='dirs -v | tail -n+2'
alias 0='popd >/dev/null; dirs -v | tail -n+2'
alias 1='pushd +1'
alias 2='pushd +2'
alias 3='pushd +3'
alias 4='pushd +4'
alias 5='pushd +5'
alias 6='pushd +6'
alias 7='pushd +7'
alias 8='pushd +8'
alias 9='pushd +9'

HISTIGNORE+=:d


hf(){
    [[ $_hf_bind ]] && {
        READLINE_LINE=$(
            _hf_bind= hf |
            tr \\n \;    |
            sed s/\;\$//
        )
        READLINE_POINT=${#READLINE_LINE}
        return
    }

    command -v fzf &> /dev/null || { history; return 1; }
    history | fzf -m +s --tac --no-mouse | sed 's/^ *[0-9]* \+//'
}
command -v fzf &> /dev/null &&
    bind -x '"\C-h":_hf_bind=1 hf "$_"'


# Coloca en la posición actual del comando que se está editando el fichero/carpeta seleccionado con fzf.
f-fzf(){
    [[ $_f_fzf_bind ]] && {
        _f_fzf_bind= f-fzf
        return
    }

    local pre post file_list file_string
    local i
    local SHELL=$(command -v bash)
    export SHELL

    [[ ${BASH_VERSION%.*} < 4.4 ]] && return 1

    pre=${READLINE_LINE:0:${READLINE_POINT:-0}}
    post=${READLINE_LINE:${READLINE_POINT:-0}}
    file_list=$(
        fzf_preview(){
            local eza_ls='ls -A'

            command -v eza &>/dev/null && {
                eza_ls="eza -ga"
            }
            eza_ls+=" -lhHF --color=always --"

            [[ -d $1 ]] && {
                ls -A -- "$1" | grep -q . || {
                    echo -e "\e[1;33mEmpty dir.\e[m"
                    return
                }
                $eza_ls "$1"
                return
            }

            # Casi igual que el "cat" del ccd().
            local tooBig taboo json text cert_req cert
            [[ -r "$1" ]] || {
                echo -ne "\e[1;31mERROR:\e[m "
                echo     "The file can not be read."
                return 1
            }
            [[ -f $1 ]] || {
                echo -ne "\e[1;31mERROR:\e[m "
                echo     "It's not a file."
                return 2
            }

            # No mostrar la preview de archivos mayores a 1M.
            tooBig=$((1*1024*1024))

            taboo='private'
            taboo+='|secret'
            taboo+='|certificate'
            taboo+='|password'

            file=$(file -L -- "$1" | sed 's/^[^:]*: //')

            command -v jq &>/dev/null && {
                json=$(grep -iq json <<<$file && echo yes)
            }
            command -v openssl &>/dev/null && {
                cert_req=$(grep -Eiq 'PEM certificate request$' <<<$file && echo yes)
                cert=$(    grep -Eiq 'PEM certificate$'         <<<$file && echo yes)
            }

            text=$(
                ! grep -Eiq "$taboo"  <<<$file &&
                grep -Eiq 'text|json' <<<$file &&
                echo yes
            )

            size=$(stat -c %s -- "$1")
            if [[ $cert ]]; then
                openssl storeutl -noout -text -certs "$1" 2>/dev/null
            elif [[ $cert_req ]]; then
                openssl req -text -noout -in "$1" 2>/dev/null
            elif [[ $size -gt $tooBig ]]; then
                echo -e "\e[1;31mFile too big (~$((size / 1024 / 1024))M).\e[m "
                echo -ne "\e[36m"
                echo -n "$file"
                echo -e "\e[m"
                if [[ $text ]]; then
                    echo -e "\e[36mtail -n50\e[m"
                    echo
                    tail -n50 < "$1"
                fi
            elif [[ $json || $text ]]; then
                grep -Eiq "$taboo" < "$1" && {
                    echo -ne "\e[36m"
                    echo -n "$file"
                    echo -e "\e[m"
                    echo -e "\e[1;33m---\e[m"
                    echo -e "\e[36mIt will not be previewed."
                    echo -ne "One of the following words was found: \e[1;31m"
                    echo -n "$taboo" | sed "s/|/, /g"
                    echo -ne "\e[m."
                    return
                }
                if [[ $json ]]; then
                    jq -C < "$1" 2>/dev/null
                else
                    cat - < "$1"
                fi
            else
                echo -ne "\e[36m"
                echo -n "$file"
            fi
        }
        export -f fzf_preview

        find              \
          -mindepth 1     \
          -maxdepth 1     |
        sed s%^\\.\\?/%%g |
        fzf               \
          --multi         \
          --cycle         \
          --preview="fzf_preview {}"
    )
    [[ $file_list ]] && {
        while IFS= read -d $'\n' i; do
            file_string+=${i@Q}' '
        done <<<$file_list
        READLINE_LINE="$pre${file_string% }$post"
        : $((READLINE_POINT += ${#file_string}))
        ((READLINE_POINT += ${#file} - 1))
    }
}
[[ ${BASH_VERSION%.*} > 4.3 ]] && command -v fzf &> /dev/null && {
    bind -x '"\C-f":_f_fzf_bind=1 f-fzf "$_"'
}


nanox(){
    [[ -e "$1" ]] ||
    if command -v termux-fix-shebang &> /dev/null; then
        echo '#!/data/data/com.termux/files/usr/bin/env bash'
    else
        echo '#!/usr/bin/env bash'
    fi >> "$1"
    ${EDITOR:-nano} "$1"
    [[ -f "$1" ]] && [[ ! -L "$1" ]] && chmod u+x "$1"
}

txzc(){
    [[ -z $1 ]] && return 1

    tar c -- "$1" |
    if command -v pv &>/dev/null; then
        pv -s $(du -bs "$1" | cut -f1)
    else
        cat -
    fi |
    xz -c > "${1%/}.txz" &&
    xz -l -- "${1%/}.txz"
}

txzx(){
    [[ -z "$1" ]] && return 1
    if command -v pv &>/dev/null; then
        pv -- "$1"
    else
        cat -- "$1"
    fi | tar xJ
}

alias bell="echo -ne '\007'"

# Visual Tmux bell.
bellt(){
    echo -n "visual-bell "
    current=$(tmux show-options -g visual-bell | cut -d\  -f2)
    if [[ "$current" == "on" ]]; then
        tmux set -g visual-bell off
        echo -e "\e[1;31moff\e[m"
    else
        tmux set -g visual-bell on
        echo -e "\e[1;32mon\e[m"
    fi
}

alias ip='ip -c'
alias ip_='{ ip -4 -o addr show;ip route show;}'
alias ip__='{ echo -e "Public ip: \e[1;35m$(curl -sfL --connect-timeout 3 https://ipinfo.io/ip)\e[m";ip_;}'
HISTIGNORE+=:ip_:ip__

alias diff='diff --color'

alias u='unset HISTFILE'
HISTIGNORE+=:u

man() {
    env \
    LESS_TERMCAP_mb=$'\e[1;31m'    \
    LESS_TERMCAP_md=$'\e[1;31m'    \
    LESS_TERMCAP_me=$'\e[m'        \
    LESS_TERMCAP_se=$'\e[m'        \
    LESS_TERMCAP_so=$'\e[1;44;33m' \
    LESS_TERMCAP_ue=$'\e[m'        \
    LESS_TERMCAP_us=$'\e[1;32m'    \
    man "$@"
}
export -f man

recv(){
    command -v ncat &> /dev/null || { echo "ncat not found" >&2; return 1; }
    local ip=${1:-127.0.0.1}
    local port=${2:-2468}
    [[ $ip =~ ^[.0-9]*$ ]] || {
    >&2 echo 'IP no valida.'
        return 1
    }

    [[ $port < 1024 ]] && {
        >&2 echo 'Puerto no valido.'
        return 1
    }
    ncat --recv-only $ip $port
}

recvl(){
    command -v ncat &> /dev/null || { echo "ncat not found" >&2; return 1; }
    local port=${1:-2468}
    [[ $port < 1024 ]] && {
        >&2 echo 'Puerto no valido.'
        return 1
    }
    ncat --recv-only -vl $port
}
send(){
    command -v ncat &> /dev/null || { echo "ncat not found" >&2; return 1; }
    local ip=${1:-127.0.0.1}
    local port=${2:-2468}
    [[ $ip =~ ^[.0-9]*$ ]] || {
    >&2 echo 'IP no valida.'
        return 1
    }

    [[ $port < 1024 ]] && {
        >&2 echo 'Puerto no valido.'
        return 1
    }
    ncat --send-only $ip $port <&0
}

sendl(){
    command -v ncat &> /dev/null || { echo "ncat not found" >&2; return 1; }
    local port=${1:-2468}
    [[ $port < 1024 ]] && {
        >&2 echo 'Puerto no valido.'
        return 1
    }
    ncat --send-only -vl $port <&0
}
export -f recv recvl send sendl


alias nvlc_='nvlc --global-key-rate-normal n --global-key-rate-slower-fine shift+, --global-key-rate-faster-fine shift+.'

genpass(){
    tr -dc "${2:-[:print:]}" < /dev/urandom | head -c ${1:-14}
    [[ ! -t 1 ]] || echo
}

transcode(){
    command -v ffmpeg &> /dev/null || {
        echo "Command ffmpeg not found." >&2
        return 1
    }

    [[ -f $1 ]] || return 1
    local audio=${an:+-an}
    audio=${audio:--c:a copy}
    nice ffmpeg           \
        -hide_banner      \
        -loglevel warning \
        -i "$1"           \
        -dn               \
        -c:v libx265      \
        $audio            \
        "${1%.*}.x265.mkv"
}
alias transcode-an='an=1 transcode'

# Tmp bash completion:
command -v rustup &>/dev/null && source <(rustup completions bash rustup;rustup completions bash cargo)
command -v helm   &>/dev/null && source <(helm completion bash)

js2yaml(){
    # Tested with yq 4.9.1.
    command -v yq &> /dev/null || return 1

    yq e -P${color:+C} ${1:--}
}

yaml2js(){
    # Tested with yq 4.9.1.
    command -v yq &> /dev/null || return 1

    yq e -j ${1:--}
}


# More personal aliases.
[[ -d ~/.bash_aliases.d ]] && {
    for i in ~/.bash_aliases.d/*; do
        . "$i"
    done
}
