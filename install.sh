#!/usr/bin/env bash

# bash <(curl -sfL https://gitlab.com/hambled/dotfiles/-/raw/main/install.sh)

# Definir el color en función de si hay terminal o no.
if [[ -t 1 || -n $DF_COLOR ]]; then
    none_c=$'\e[m'
     yes_c=$'\e[1;32m'
     not_c=$'\e[1;31m'
    item_c=$'\e[36m'
    info_c=$'\e[1;36m'
     err_c=$'\e[1;31m'
     war_c=$'\e[1;33m'
else
    none_c=
     yes_c=
     not_c=
    item_c=
    info_c=
     err_c=
     war_c=
fi
info="$info_c[INFO]$none_c"
err="$err_c[ERROR]$none_c"
war="$war_c[WARNING]$none_c"

# Obligamos a que el script corra con bash.
[[ $BASH_VERSION ]] || {
    >&2 echo "$err Este script está pensado para ser usado con ${item_c}bash$none_c."
    exit 1
}

# Si no estamos en una TTY tomaremos todas las opciones por defecto.
[[ -t 0 ]] || DF_FORCE=1
QUESTION_TIMEOUT=${DF_FORCE:+0}

repository=https://gitlab.com/hambled/dotfiles

files=(
    # Recomendados:
    .tmux.conf
    .bashrc
    .bash_aliases
    .bash_aliases.d/bash_jump
    .bash_aliases.d/ccd
    .bash_aliases.d/command-fzf
    .bash_aliases.d/grep-ip
    .bash_aliases.d/paleta256
    .bash_aliases.d/pi
    .bash_aliases.d/PS1_builder
    .bash_aliases.d/show_remote_cert
    .bash_aliases.d/uc
    .bash_aliases.d/up_

    # Opcional:
    opt#bin/tmux-conf-ssh
    opt#bin/tmux-vi-conf
    opt#bin/fast_bash_docker_tmux
    opt#bin/fast_bash_McA
    opt#bin/listarFicheros.sh
    opt#bin/renombrarFicheros.sh
    opt#.bash_aliases.d/bashrc_cka
    opt#.bash_aliases.d/kexplain
    opt#.bash_aliases.d/tsf
    opt#.bash_aliases.d/fix_command-fzf
    opt#.gitconfig.d/aliases.conf
)

commands=(
    fzf
    xz
    openssl
    tput
    sed
    tmux
    man
    stty
    kubectl
    strings
    file
    diff
    curl
)

# Función auxiliar para realizar preguntas al usuario.
questionYN(){
    local opt=.
    local SN SNP
    local timeout=${QUESTION_TIMEOUT//[^0-9]/}

    if [[ "${2,,}" == n ]]; then
        SN=n
        SNP='(y/N)'
    else
        SN=y
        SNP='(Y/n)'
    fi
    printf '%s %s: ' "$1" "$SNP"
    while [[ "${opt//[ynYN]/}" ]]; do
        read -srn1 ${timeout:+ -t$timeout} opt
        [[ $? -eq 0 && $opt ]] || opt="$SN"
        case ${opt,} in
            y) echo "${yes_c}y$none_c";;
            n) echo "${not_c}n$none_c";;
        esac
    done
    [[ ${opt,} == y ]]
}

: "${DF_PATH:=$HOME/git/McA/dotfiles}"


# Asegurarse de que los ficheros a instalar están disponibles.
for file in "${files[@]}"; do
    [[ ${file%#*} == opt ]] && continue
    [[ -f "$DF_PATH/$file" ]] || ! break
done || {
    >&2 echo "$info No se encontró alguno de los ficheros del dotfiles."
    questionYN "¿Instalar desde GitLab en $item_c$DF_PATH$none_c?" n || exit

    [[ -d $DF_PATH ]] && {
        echo "Parece que $item_c$DF_PATH$none_c ya existe."
        questionYN "¿Seguro que deseas continuar?" n || exit
    }

    # Clonar o terminar con el mismo código de error que ha dado.
    git clone "$repository" "$DF_PATH" || exit
}
DF_PATH=$(realpath "$DF_PATH")


for file in "${files[@]}"; do
    dst="$HOME/${file#*#}"
    src="$DF_PATH/${file#*#}"

    diff "$src" "$dst" &>/dev/null && continue

    [[ ${file%#*} == opt ]] &&
    ! questionYN "¿Instalar $item_c${file#*#}$none_c?" n && continue

    [[ -f "$dst" ]] &&
    ! questionYN "¿Reemplazar $item_c$dst$none_c por el del dotfiles?" n &&
    continue

    mkdir -p "${dst%/*}"
    >&2 echo "$info Creando: $item_c$dst$none_c."
    ln -sf "$src" "$dst"
done

no_cmd=
for cmd in "${commands[@]}"; do
    command -v "$cmd" &>/dev/null && continue
    >&2 echo "$war No se encontró el comando ${item_c}$cmd$none_c."
    no_cmd=1
done

[[ $no_cmd ]] &&
>&2 echo "$war Se recomienda instalar los comandos anteriores para asegurar el buen funcionamiento."
