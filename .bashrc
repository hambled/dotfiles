# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

HISTCONTROL=ignoreboth
HISTIGNORE="ll:ls"
HISTIGNORE+=":df -h"
HISTIGNORE+=":reset"
HISTIGNORE+=":clear"
HISTIGNORE+=":history"
HISTIGNORE+=":pwd"
HISTIGNORE+=":date:cal"
HISTIGNORE+=":uptime"
HISTIGNORE+=":sync"
HISTIGNORE+=":htop"
HISTIGNORE+=":id"
HISTIGNORE+=":hostnamectl"
HISTIGNORE+=":who:w"

HISTIGNORE+=":git status"
HISTIGNORE+=":git diff"
HISTIGNORE+=":git log"
HISTIGNORE+=":git logdate" # Git aliases
HISTIGNORE+=":git lodag"   # Git aliases

HISTIGNORE+=":svn status"
HISTIGNORE+=":svn info"

HISTIGNORE+=":code"
HISTIGNORE+=":code ."

HISTIGNORE+=":subl"
HISTIGNORE+=":subl ."

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=20000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files.
# Eg: less file.tar
# Eg: less file.deb
[[ -x /usr/bin/lesspipe ]] && eval "$(SHELL=/bin/sh lesspipe)"


# Mecanismo para resetear el color establecido en el input de la línea de
# comandos en caso de que esté establecida la variable PS1_INPUT_COLOR.
# HOTFIX: Esta función está así por varios motivos.
# 1. El primero es evitar imprimir más veces de las necesarias la secuencia '\e[m'.
# 2. Solucionar un problema anterior por el cual no se reseteaba correctamente el
#    color tras el uso de un binding.
ps1_reset_input_color(){
    [[ $PS1_INPUT_COLOR_CONTROL ]] && {
        echo -ne '\e[m'

        # Si está definido, es un binding.
        declare -p READLINE_LINE &> /dev/null ||
        unset PS1_INPUT_COLOR_CONTROL
    }
}

ps1_timer_start(){
    BASH_PS1_TIMER=${BASH_PS1_TIMER:-$SECONDS}
}

ps1_timer_stop(){
    local t=$(($SECONDS - $BASH_PS1_TIMER))
    unset timer_show
    unset BASH_PS1_TIMER
    [[ $t -ge 86400 ]] && timer_show+=$((t / 86400))d; t=$((t % 86400))
    [[ $t -ge 3600  ]] && timer_show+=$((t / 3600 ))h; t=$((t % 3600 ))
    [[ $t -ge 60    ]] && timer_show+=$((t / 60   ))m
    timer_show+=$((t % 60))s
}

trap 'ps1_reset_input_color "$_";ps1_timer_start "$_"' DEBUG


if [[ -z $PROMPT_COMMAND ]]; then
    PROMPT_COMMAND="ps1_timer_stop"
else
    PROMPT_COMMAND="$PROMPT_COMMAND; ps1_timer_stop"
fi

PS1_DEFAULT_COLOR=$([[ $EUID -eq 0 ]] && echo -ne "\e[;1;31m" || echo -ne "\e[;33m")
PS1_NONE_COLOR=$(    echo -ne "\e[m"      )
PS1_DATE_COLOR=$(    echo -ne "\e[1;32m"  )
PS1_BANNER_COLOR=$(  echo -ne "\e[1;5;31m")
PS1_RETVAL_COLOR=$(  echo -ne "\e[1;31m"  )
PS1_DIRS_COLOR=$(    echo -ne "\e[36m"    )
PS1_EXECTIME_COLOR=$(echo -ne "\e[m"      )
PS1_WORKDIR_COLOR=$( echo -ne "\e[1;35m"  )
PS1_NOTHIST_COLOR=$( echo -ne "\e[1;32m"  )
PS1_USER_COLOR=$(    echo -ne "\e[1;32m"  )
PS1_HOST_COLOR=$(    echo -ne "\e[1;32m"  )
PS1_INPUT_COLOR=$(   echo -ne ""          ) # BUG: No se resetea el color con las opciones mostradas en el autocompletado.
PS1_AT_COLOR=$PS1_DEFAULT_COLOR
PS1_UID_COLOR=$PS1_DEFAULT_COLOR

# [0s][1][BANNER]130↵
# [~]$† echo Saluton!
PS1_OPT1="\[$PS1_NONE_COLOR"                                                    #
PS1_OPT1+="\$PS1_DEFAULT_COLOR\][\[$PS1_NONE_COLOR"                             # [
PS1_OPT1+="\$PS1_EXECTIME_COLOR\]\$timer_show\[$PS1_NONE_COLOR"                 # 0s
PS1_OPT1+="\$PS1_DEFAULT_COLOR\]]\[$PS1_NONE_COLOR"                             # ]
PS1_OPT1+='$(((E=$?,d=$(dirs -v|wc -l)-1));[[ ${d/0/} ]]&&'                     # (Preservar el valor de $?).
  PS1_OPT1+="echo \"\$PS1_DIRS_COLOR\][\$d]\[$PS1_NONE_COLOR\";exit \$E)"       # [1] (Solo si dirs != 0)
PS1_OPT1+='${PS1_BANNER:+$PS1_BANNER_COLOR\][$PS1_BANNER]'"\[$PS1_NONE_COLOR}"  # [BANNER]
PS1_OPT1+='$(E=$?;[[ ${E/0/} ]]&&'                                              # (Preservar el valor de $?).
  PS1_OPT1+='echo "$PS1_RETVAL_COLOR\]$E\342\206\265'"\[$PS1_NONE_COLOR\")"     # 130↵ (Solo si $? != 0).
PS1_OPT1+='\]\n\['                                                              #
PS1_OPT1+="\$PS1_DEFAULT_COLOR\][\[$PS1_NONE_COLOR"                             # [
PS1_OPT1+="\$PS1_WORKDIR_COLOR\]\w\[$PS1_NONE_COLOR"                            # ~
PS1_OPT1+="\$PS1_DEFAULT_COLOR\]]\[$PS1_NONE_COLOR"                             # ]
PS1_OPT1+="\$PS1_UID_COLOR\]\\\$\[$PS1_NONE_COLOR"                              # $
PS1_OPT1+='$([[ $HISTFILE ]]||'                                                 # (Si no $HISTFILE....)
  PS1_OPT1+='echo "$PS1_NOTHIST_COLOR\]\742\600\640'"\[$PS1_NONE_COLOR\")"      # †
PS1_OPT1+='\]'                                                                  #
PS1_OPT1+="\[\${PS1_INPUT_COLOR_CONTROL:=\$PS1_INPUT_COLOR}\] "                 # echo Saluton!

# [BANNER]130↵
# [23:59:59][0s][1][user@hostname:~]$† echo Saluton!
PS1_OPT2="\[$PS1_NONE_COLOR"                                                     #
PS1_OPT2+='${PS1_BANNER:+$PS1_BANNER_COLOR\][$PS1_BANNER]'"\[$PS1_NONE_COLOR}"   # [BANNER]
PS1_OPT2+='$(E=$?;[[ ${E/0/} ]]&&'                                               # (Preservar el valor de $?).
  PS1_OPT2+="echo -n \"\$PS1_RETVAL_COLOR\]\$E\342\206\265\[$PS1_NONE_COLOR\"||" # 130↵ (Solo si $? != 0).
  PS1_OPT2+='[[ $PS1_BANNER ]] && echo "\]\n\[" || echo "\[")'                   # \n (solo si hay BANNER o $? != 0). # Nota: En el `echo`, es necesario que exista algo después del \n para que no se ignore.
PS1_OPT2+="\$PS1_DEFAULT_COLOR\][\[$PS1_NONE_COLOR"                              # [
PS1_OPT2+="\$PS1_DATE_COLOR\]\t\[$PS1_NONE_COLOR"                                # 23:59:59
PS1_OPT2+="\$PS1_DEFAULT_COLOR\]][\[$PS1_NONE_COLOR"                             # ][
PS1_OPT2+="\$PS1_EXECTIME_COLOR\]\$timer_show\[$PS1_NONE_COLOR"                  # 0s
PS1_OPT2+="\$PS1_DEFAULT_COLOR\]]\[$PS1_NONE_COLOR"                              # ]
PS1_OPT2+='$(((d=$(dirs -v|wc -l)-1));[[ ${d/0/} ]]&&'                           # (Si hay dirs...)
  PS1_OPT2+="echo \"\$PS1_DIRS_COLOR\][\$d]\[$PS1_NONE_COLOR\")"                 # [1]
PS1_OPT2+="\$PS1_DEFAULT_COLOR\][\[$PS1_NONE_COLOR"                              # [
PS1_OPT2+="\$PS1_USER_COLOR\]\u\[$PS1_NONE_COLOR"                                # user
PS1_OPT2+="\$PS1_AT_COLOR\]@\[$PS1_NONE_COLOR"                                   # @
PS1_OPT2+="\$PS1_HOST_COLOR\]\h\[$PS1_NONE_COLOR"                                # hostname
PS1_OPT2+="\$PS1_DEFAULT_COLOR\]:\[$PS1_NONE_COLOR"                              # :
PS1_OPT2+="\$PS1_WORKDIR_COLOR\]\w\[$PS1_NONE_COLOR"                             # ~
PS1_OPT2+="\$PS1_DEFAULT_COLOR\]]\[$PS1_NONE_COLOR"                              # ]
PS1_OPT2+="\$PS1_UID_COLOR\]\\\$\[$PS1_NONE_COLOR"                               # $
PS1_OPT2+='$([[ $HISTFILE ]]||'                                                  # (Si no $HISTFILE....)
  PS1_OPT2+='echo "$PS1_NOTHIST_COLOR\]\742\600\640'"\[$PS1_NONE_COLOR\")"       # †
PS1_OPT2+='\]'                                                                   #
PS1_OPT2+="\[\${PS1_INPUT_COLOR_CONTROL:=\$PS1_INPUT_COLOR}\] "                  # echo Saluton!

# ┌─[0s]─[user@hostname]─[~][1][BANNER]130↵
# └──╼ $† echo Saluton!
PS1_OPT3="\[$PS1_NONE_COLOR"                                                   #
PS1_OPT3+="\$PS1_DEFAULT_COLOR\]\342\224\214\342\224\200[\[$PS1_NONE_COLOR"    # ┌─[
PS1_OPT3+="\$PS1_EXECTIME_COLOR\]\$timer_show\[$PS1_NONE_COLOR"                # 0s
PS1_OPT3+="\$PS1_DEFAULT_COLOR\]]\342\224\200[\[$PS1_NONE_COLOR"               # ]─[
PS1_OPT3+="\$PS1_USER_COLOR\]\u\[$PS1_NONE_COLOR"                              # user
PS1_OPT3+="\$PS1_AT_COLOR\]@\[$PS1_NONE_COLOR"                                 # @
PS1_OPT3+="\$PS1_HOST_COLOR\]\h\[$PS1_NONE_COLOR"                              # hostname
PS1_OPT3+="\$PS1_DEFAULT_COLOR\]]\342\224\200[\[$PS1_NONE_COLOR"               # ]─[
PS1_OPT3+="\$PS1_WORKDIR_COLOR\]\w\[$PS1_NONE_COLOR"                           # ~
PS1_OPT3+="\$PS1_DEFAULT_COLOR\]]\[$PS1_NONE_COLOR"                            # ]
PS1_OPT3+='$(((E=$?,d=$(dirs -v|wc -l)-1));[[ ${d/0/} ]]&&'                    # (Preservar el valor de $?).
  PS1_OPT3+="echo \"\$PS1_DIRS_COLOR\][\$d]\[$PS1_NONE_COLOR\";exit \$E)"      # [1] (Solo si dirs != 0)
PS1_OPT3+='${PS1_BANNER:+$PS1_BANNER_COLOR\][$PS1_BANNER]'"\[$PS1_NONE_COLOR}" # [BANNER]
PS1_OPT3+='$(E=$?;[[ ${E/0/} ]]&&'                                             # (Preservar el valor de $?).
  PS1_OPT3+='echo "$PS1_RETVAL_COLOR\]$E\342\206\265'"\[$PS1_NONE_COLOR\")"    # 130↵ (Solo si $? != 0).
PS1_OPT3+='\]\n\[$PS1_DEFAULT_COLOR\]'                                         #
PS1_OPT3+="\342\224\224\342\224\200\342\224\200\342\225\274 \[$PS1_NONE_COLOR" # └──╼
PS1_OPT3+="\$PS1_UID_COLOR\]\\\$\[$PS1_NONE_COLOR"                             # $
PS1_OPT3+='$([[ $HISTFILE ]]||'                                                # (Si no $HISTFILE....)
  PS1_OPT3+='echo "$PS1_NOTHIST_COLOR\]\742\600\640'"\[$PS1_NONE_COLOR\")"     # †
PS1_OPT3+="\${PS1_INPUT_COLOR_CONTROL:=\$PS1_INPUT_COLOR}\] "                  # echo Saluton!

PS1=$PS1_OPT1


# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [[ -f ~/.bash_aliases ]]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
    if [[ -f /usr/share/bash-completion/bash_completion ]]; then
        . /usr/share/bash-completion/bash_completion
    elif [[ -f /etc/bash_completion ]]; then
        . /etc/bash_completion
    fi
fi

shopt -s histverify
shopt -s autocd
