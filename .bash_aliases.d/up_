# Función para construir una secuencia tuberías partiendo de la salida de un comando.
# Está pensado para poder ser ejecutado con C-xC-p ('P' de 'Pipe').
#
# El comando se compone de dos areas, el lado izquierdo (LS) y el lado derecho (RS).
# Tanto LS como RS puede ser cualquier secuencia de comandos.
# Puede interpretarse su ejecución como el resultado de ejecutar:
# { <LS>; } | { <RS> }
#
# Hay dos puntos importantes a mencionar:
# 1. Si el RS empieza por uno de los siguientes carácteres [#;>], la interpretación pasa a ser
#    una de las siguientes:
#    { <LS>; } # <RS>
#    { <LS>; };  <RS>
#    { <LS>; } > <RS>
#
# 2. Tanto el LS como el RS se procesan por separado, es decir, primero se procesará el LS hasta
#    que este finalice, y sobre la salida de este se procesará el RS.
#    Es importante entender esto, ya que si el LS es un comando que no termina nunca, el RS nunca
#    se procesará y el programa se quedará colgado.
#
# Esta función se encuentra fuertemente inspirada en la aplicación "Ultimate Plumber".
# Ver: https://github.com/akavel/up
#
# Atajo de teclas:
# <Esc>        : Salir.
# <Ctrl>-l     : Ejecutar la linea actual y ver la salida en un comando `less`.
# <PagUp>      : Igual que <Ctrl>-l.
# <PagDown>    : Igual que <Ctrl>-l.
# <Ctrl>-r     : Cambia temporalmente el lado derecho por un `cat` (Salida "raw").
# <Ctrl>-p     : (Print on exit) Imprime el resultado final al terminar el comando.
# <Ctrl>-a     : Conmuta entre el lado izquierdo y el derecho.
# <Ctrl>-e     : (Des)activa la salida de STDERR (no recarga la ejecución).
# <Ctrl>-<Esp> : El cursor vuelve a la anterior posición.
# <Enter>      : Ejecutar la línea actual.
#                Nota: Si la línea está vacía, se termina.
#
# FIXME:
# He notado problemas cuando se ejecuta como una tubería en vez de con atajo de teclado, es decir algo así:
# `: | up_` o bien `up_ < <(:)`
# Esto abrirá el up_ en el *RS*.
# Ahora ejecutamos un comando que cueste tiempo procesar. Por ejemplo: `sleep 5`.
# Mientras se procesa, pulsamos `ctrl-c` para cancelar.
# El up_ detecta la señan y muestra el mensaje de interrupción, pero el lugar de volver al prompt del up_, pone el
# proceso en segundo plano y vuleve a la shell.
# Es posible retomar el up_ ejecutando `fg` y reanuda "normalmente" la función.
#
# ToDo:
# [ ] Mejorar la visualización del prompt.
#   [ ] Añadir alguna referencia visual para saber si hace falta refrescar la salida o no.
# [-] Conservar la columna del cursor en el comando.
#     Nota: No he podido hacerlo automático, hace falta pulsar el C-<Espacio>.
# [ ] Permitir construcciones más complejas, con varios pipes e incluso con ramificaciones.
#
# Nota sobre los estados:
# | $up_subcmd | Lado- | Lado+ |            Nota             | Nota 2
# |:-----------|:-----:|:-----:|:----------------------------|:-------
# | <nada>     |  RS   |  RS   | Permanecer en RS.           | Se procesa el RS.
# | rev        |  RS   |  LS   | Entrar en el LS.            | Se procesa el RS si es distinto del anterior.
# | rev2       |  LS   |  LS   | Permanecer en LS.           | Se procesa el LS y el RS.
# | rev3       |  LS   |  RS   | Entrar en el RS.            | Se procesa el LS y el RS si LS es distinto del anterior.
# | cat_*      |   -   |   -   | Mostrar LS sin procesar RS. | Solo se muestra el LS (sin procesar nada).
# | less_*     |   -   |   -   | Paginar salida actual.      | Se procesa el RS si es distinto del anterior.
# | print_*    |   -   |   -   | Imprime al terminar.        | Conmuta la variable.
#
up_(){
    trap '' SIGINT
    # Bloque solo para el keybinding.
    [[ $_up_bind == 1 ]] && {
        local stty tmp
        stty=$(stty -g)
        _up_bind=2
        READLINE_LINE=$(up_ < /dev/null)
        stty "$stty"
        READLINE_POINT="${#READLINE_LINE}"
        trap - SIGINT
        return
    }


    # Parte principal de la función.

    # No queremos que los bind ni las funciones sigan definidos cuando el
    # up_() termine, pero tampoco queremos tener que destruirlos a mano.
    # Por tanto, la opción más simple es encerrarlos dentro de una subshell.
    # Además esto permite recuperar la memoria consumida en las variables
    # $up_input y $up_output, que pude llegar a ser muy grande.
    (
        trap '' SIGINT

        local HISTCONTROL=ignoreboth
        local up_subcmd
        local up_compress
        local up_output
        local up_input
        local up_cancel
        local up_cmd up_last_cmd
        local up_tput
        local up_prompt='|'
        local up_prompt2
        local up_prompt_err='[err]'
        local up_pipe_rs
        local up_rll=$READLINE_LINE
        local up_rll_ls
        local up_rll_rs
        local up_stderr_ls=1
        local up_stderr_rs
        local up_print_on_exit
        local up_prompt_bg
        local up_hist_ls=()
        local up_hist_rs=()
        export HISTCONTROL

        # Restaurar algunos keybindings.
        bind '"\C-f":forward-char'
        bind '"\C-g":abort'
        bind -r "\C-k"   # clear-display.
        bind -r "\C-h"   # hf().

        command -v bash_jump &>/dev/null && {
            bind -x '"\C-g":"PS1=$up_prompt2$up_prompt\  BJ_DEFAULT_BG=$up_prompt_bg bash_jump"'
        }

        # Salir.
        up_kb_exit(){
            if [[ $up_subcmd == rev2 ]]; then
                printf -v up_rll_ls "$READLINE_POINT"
                up_subcmd=rev3
            else
                up_subcmd=exit
            fi
        }
        bind -x '"\C-h1":"up_kb_exit"'
        bind    '"\e":"\C-h1\C-j"'

        # Volver a la posición anterior.
        up_kb_get_posicion(){
            [[ $READLINE_POINT -eq ${#READLINE_LINE} ]] && return
            case $up_subcmd in
                *rev2) up_rll_ls=$READLINE_POINT;;
                *    ) up_rll_rs=$READLINE_POINT;;
            esac
        }
        up_kb_set_posicion(){
            case $up_subcmd in
                *rev2) READLINE_POINT=$up_rll_ls;;
                *    ) READLINE_POINT=$up_rll_rs;;
            esac
        }
        bind -x '"\C-h2":"up_kb_get_posicion"'
        bind    '"\C-m":"\C-h2\C-j"'
        bind -x '"\C-\ ":"up_kb_set_posicion"'

        # Pagina la salida actual con `less`.
        bind -x '"\C-h3":"up_subcmd=less_$up_subcmd"'
        bind    '"\C-l":"\C-h3\C-m"'
        bind    '"\e[5~":"\C-h3\C-m"'
        bind    '"\e[6~":"\C-h3\C-m"'

        if [[ $_up_bind == 2 ]]; then
            # Intercambia entre el RS y el LS.
            up_kb_intercambio(){
                case $up_subcmd in
                    rev2) up_subcmd=rev3;;
                    rev3) up_subcmd=    ;;
                    *   ) up_subcmd=rev ;;
                esac
            }
            bind -x '"\C-h4":"up_kb_intercambio"'
            bind    '"\C-a":"\C-h2\C-h4\C-j"'
        else
            bind -r "\C-a"
        fi

        # (Raw) Mostrar la última salida del LS (sin procesarla con el RS).
        bind -x '"\C-h5":"up_subcmd=cat_$up_subcmd"'
        bind    '"\C-r":"\C-h5\C-m"'

        # (Des)activar el mostrar la salida del comando al salir.
        bind -x '"\C-h6":"up_subcmd=print_$up_subcmd"'
        bind    '"\C-p":"\C-h6\C-m"'

        # (Des)activar el mostrar la salida del comando al salir.
        bind -x '"\C-h7":"up_subcmd=err_$up_subcmd"'
        bind    '"\C-e":"\C-h7\C-m"'


        if   command -v zstd  ;then up_compress=zstd
        elif command -v gzip  ;then up_compress=gzip
        elif command -v xz    ;then up_compress=xz
        elif command -v lbzip2;then up_compress=lbzip2
        fi &>/dev/null

        up_extract_stream(){
            local stream
            declare -n stream=$1
            [[ ${stream[@]} ]] || return

            printf '\0%s' "${stream[@]}" |
            eval "tail -c+2 ${up_compress:+|$up_compress -d}"
        }

        up_reset_hist(){
            local cmd

            history -c

            : "$1[@]"
            for cmd in "${!_}"; do
                history -s -- "$cmd"
            done
        }

        up_add_hist(){
            history -s -- "$1"
            case $up_subcmd in
                *rev[23]) up_hist_ls+=("$1");;
                *       ) up_hist_rs+=("$1");;
            esac
        }

        up_set_output(){
            [[ $up_cancel -eq 1 ]] && {
                up_cancel=2
                return
            }

            echo -n $'\e[38;5;33mProcessing right side...\e[m'
            trap 'echo;echo -e "\e[38;5;33mProcess interrupted by user.\e[m";sleep 2' SIGINT
            readarray -td '' up_output < <(
                trap 'exit' SIGINT
                local cmd
                if   [[   $1 =~ ^[\;\>] ]]; then
                    cmd="cat$1"
                elif [[ ! $1 =~ ^\ *#   ]]; then
                    # Si fuese '#.*' sería el mismo comportamiento que el `C-r`.
                    cmd=$1
                fi

                # [FIX] Es necesario añadir un salto de línea para evitar que,
                # entradas como la siguiente, puedan interferir en el `eval`:
                # `cat # Sin el salto de línea, este comentario rompe el programa.`
                < <(up_extract_stream up_input) \
                eval "{ ${cmd:-cat}
                } 2>/dev/null${up_stderr_rs:+ 2>&1}${up_compress:+|$up_compress}"

                # [HOTFIX]: Por algún motivo, `readarray` no toma el último valor si la entrada termina con delimitador.
                # Así que forzamos a que exista un delimitador extra que será descartado por el `readarray`.
                # Esto permite preserva el último caracter sea delimitador o no.
                printf '\0'
            )
            trap '' SIGINT
            up_pipe_rs=$1
        }

        up_set_input(){
            up_cancel=

            [[ $_up_bind == 2 ]] &&
            echo -n $'\e[38;5;40mProcessing left side...\e[m'

            trap "up_input= up_cancel=1;echo;echo -e '\e[38;5;40mProcess interrupted by user.\e[m';sleep 2" SIGINT
            readarray -td '' up_input < <(
                trap 'exit' SIGINT
                eval "{ ${1:-:}
                } 2>/dev/null${up_stderr_ls:+ 2>&1}${up_compress:+|$up_compress}"
                printf '\0'
            )
            trap '' SIGINT
            echo
            up_set_output "$up_pipe_rs"
            up_rll=$1
        }

        up_cat(){
            local up_pipe_rs
            up_set_output
            up_subcmd=${up_subcmd#cat_}
        }

        up_less(){
            clear
            up_extract_stream up_output |
            less -XS
            up_subcmd=${up_subcmd#less_}
        }

        # Entrar al lado izquierdo.
        up_rev(){
            up_reset_hist up_hist_ls
            up_subcmd=rev2
            [[ $up_pipe_rs != $1 ]] && {
                up_set_output "$1"
            }
            up_last_cmd=$up_rll
            up_prompt2=${up_stderr_ls:+"$up_prompt_err"}
        }

        # Seguir en el lado izquierdo.
        up_rev2(){
            if [[ $1 ]]; then
                up_set_input "$1"
                up_rll=$1
            else
                up_rev3 "$up_rll"
            fi
        }

        # Volver al lado derecho.
        #
        # FIXME: Cuando se cancela el comando del LS no hay que volver al RS.
        up_rev3(){
            up_reset_hist up_hist_rs
            up_subcmd=
            [[ $1 && $up_rll != $1 && ! $up_cancel ]] && {
                up_rll=$1
                up_set_input "$up_rll"
            }
            up_last_cmd=$up_pipe_rs
            up_prompt2=${up_stderr_rs:+"$up_prompt_err"}
        }

        up_normal(){
            up_set_output "$1"
        }

        up_set_prompt(){
            clear
            echo -ne "\e[${LINES}f"
            {
                up_extract_stream up_output
                # FIX: Este `echo` y el `sed` siguiente, se usan para garantizar que la última línea
                # termine siempre con un salto de línea.
                # Esto impide que la última línea de la salida se pueda imprimir en la misma línea
                # del prompt, de esta forma, el prompt siempre queda en una línea independiente.
                echo
            } |
            sed '${/^$/d;s/$/\x1b[;31m$\x1b[m/}' |
            tail -n${LINES:-50}

            if [[ ${up_subcmd%2} == rev ]]; then
                up_prompt_bg=28
                echo -ne "\e[;48;5;28m\e[K" # Imprime el fondo para el prompt.
            else
                up_prompt_bg=21
                echo -ne "\e[;48;5;21m\e[K" # Imprime el fondo para el prompt.
            fi
        }

        up_switch_print(){
            up_subcmd=${up_subcmd#print_}
            if [[ $up_print_on_exit ]]; then
                up_print_on_exit=
                echo -n $'\e[1;31mDo not print on exit.\e[m'
            else
                up_print_on_exit=1
                echo -n $'\e[1;32mPrint on exit.\e[m'
            fi
            sleep 2
        }

        up_switch_stderr(){
            up_subcmd=${up_subcmd#err_}

            case $up_subcmd in
                *rev2) [[ $up_stderr_ls ]] && up_stderr_ls= up_prompt2= || up_stderr_ls=1 up_prompt2="$up_prompt_err" ;;
                *    ) [[ $up_stderr_rs ]] && up_stderr_rs= up_prompt2= || up_stderr_rs=1 up_prompt2="$up_prompt_err" ;;
            esac
        }

        up_help(){
            echo -e '\e[4mAtajos de teclado:\e[m'
            command -v bash_jump &>/dev/null &&
            echo -e '\e[36mCtrl-g\e[m  : \e[3mbash_jump()\e[m.'
            echo -e '\e[36mCtrl-Esp\e[m: Anterior posición.'
            echo -e '\e[36mCtrl-p\e[m  : \e[3m(Print)\e[m  Imprimir el \e[38;5;21mlado drch\e[m al salida sin ejecutarlo.'
            echo -e '\e[36mCtrl-r\e[m  : \e[3m(Raw)\e[m    Muestra el \e[38;5;28mlado izq\e[m sin ejecutarlo.'
            echo -e '\e[36mCtrl-a\e[m  : \e[3m(Alter)\e[m  Conmutar entre lado \e[38;5;28mizq\e[m y \e[38;5;21mdrch\e[m.'
            echo -e '\e[36mCtrl-e\e[m  : \e[3m(STDERR)\e[m (Des)activa la salida del <STDERR> (\e[36m2>&1\e[m).'
            echo -e '\e[36mCtrl-l\e[m  : \e[3m(less)\e[m   Paginado con \e[36mless\e[m.'
            echo -e '\e[36mPagUp\e[m   : \e[3m(less)\e[m   Paginado con \e[36mless\e[m.'
            echo -e '\e[36mPagDown\e[m : \e[3m(less)\e[m   Paginado con \e[36mless\e[m.'
            echo -e '\e[1;31mEsc\e[m     : \e[3mExit\e[m.'
            read -sn1 -t2 < /proc/$$/fd/0
        }

        # Para que el programa no interfiera con el resultado final, hace falta
        # usar un FD distinto del STDOUT, por ejemplo el STDERR.
        # Para no tener que redireccionar cada comando a STDERR, encerramos el
        # bloque entero entre llaves con una redirección al final.
        {
            command -v tput &>/dev/null && up_tput=1
            # Almacena el terminal previo y pasa a un modo especial.
            if [[ $tput ]]; then
                tput smcup
            else
                echo -n $'\E[?1049h'
            fi

            echo -ne "\e[${LINES}f"     # Bajar a la parte inferior.
            [[ $UPNOHELP ]] || up_help
            if [[ $_up_bind == 2 ]]; then
                if [[ $up_rll ]]; then
                    up_set_input "$up_rll"
                else
                    up_rll=$(fc -ln | tail -n1 | sed 's/^[[:space:]]*//g')
                    : ${up_rll:='echo "Saluton ${USER^}! Kiel vi fartas?"'}
                    up_rev
                fi
            else
                up_set_input cat
            fi

            history -c

            up_set_prompt
            while IFS= read -rep "$up_prompt2$up_prompt " -i "$up_last_cmd" up_cmd; do
                # Saneamiento de los posible '|' al principio de línea.
                [[ $up_cmd =~ ^[\ \|]*(.*) ]]
                # Saneamiento de los posible '|' al final de línea.
                [[ ${BASH_REMATCH[1]} =~ ^(.*[^\ \|])[\ \|]*$ ]]
                up_cmd=${BASH_REMATCH[1]}

                [[ $up_cmd != $up_last_cmd ]] &&
                    up_add_hist "$up_last_cmd"
                up_last_cmd=$up_cmd
                echo -ne '\e[m\e[K'
                [[ $up_cmd || $up_subcmd ]] || break

                case $up_subcmd in
                less_* ) up_less                  ;;
                cat_*  ) up_cat                   ;;
                rev    ) up_rev  "$up_cmd"        ;;
                rev2   ) up_rev2 "$up_cmd"        ;;
                rev3   ) up_rev3 "$up_cmd"        ;;
                exit   ) up_pipe_rs=$up_cmd; break;;
                print_*) up_switch_print          ;;
                err_*  ) up_switch_stderr         ;;
                *      ) up_normal "$up_cmd"      ;;
                esac

                up_set_prompt
            done < /dev/tty

            if [[ $up_tput ]]; then tput rmcup
            else echo -n $'\E[?1049l'
            fi

            [[ $up_print_on_exit ]] && {
                up_extract_stream up_output
                echo
            } |
            sed '${/^$/d;s/$/\x1b[;31m$\x1b[m/}'
        } >&2

        # Construcción de la línea final.
        [[   $up_rll                ]] && printf %s "$up_rll"
        [[   $up_rll && $up_pipe_rs ]] && {
            if [[ $up_pipe_rs =~ ^[\>#] ]]; then
                printf ' '
            elif [[ ! $up_pipe_rs =~ ^\; ]]; then
                printf ' | '
            fi
        }
        [[              $up_pipe_rs ]] && printf %s "$up_pipe_rs"
        [[ ! $up_rll && $up_pipe_rs ]] && echo
    )
    trap - SIGINT
}

bind -x '"\C-x\C-p":_up_bind=1 up_ "$_"'
